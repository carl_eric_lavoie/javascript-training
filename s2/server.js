// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var path 	   = require('path');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;        // set our port


// Database config
var firebase = require("firebase");
firebase.initializeApp({
	databaseURL: "https://javascript-training-142815.firebaseio.com/",
	serviceAccount: "credentials.json"
});
var db = firebase.database();
var messagesRef = db.ref("messages");

var Message     = require('./app/models/message');

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// more routes for our API will happen here
router.route('/messages')
		.post(function(req, res) {

			var message = new Message();
			message.txt = req.body.txt;
			message.user = req.body.user;
			message.timestamp = Date.now();
			messagesRef.push().set(message, function(error) {
				if (error) {
					res.send(error);
				} else {
					res.send("success");
				}
			});
		})

		.get(function(req, res){
			messagesRef.once("value", function(snapshot) {
				const messages = [];
				var messagesObj = snapshot.val();
				for(key in messagesObj){
					messages.push(messagesObj[key]);
				}
				res.json(messages);
			}, function (errorObject) {
				console.log("The read failed: " + errorObject.code);
			});
		});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);



// START THE SERVER
// =============================================================================
app.listen(port);

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/src/:name', function(req, res){
	var fileName = req.params.name;
	res.sendFile(path.join(__dirname + '/src/'+ fileName));
})

app.get('/styles/:name', function(req, res){
	var fileName = req.params.name;
	res.sendFile(path.join(__dirname + '/styles/'+ fileName));
})
