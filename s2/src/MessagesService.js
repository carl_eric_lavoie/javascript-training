function MessagesService() {
	var messagesRef = config.fb.child("messages");

	this.addMessage = function (text) {
		var msg = new Message(text, authenticationService.currentUser, Date.now());
		$.post({
			url : '/api/messages',
			data : msg,
			success: function(val, error){
				this.getMessages();
			}.bind(this)
		});
		//this.showMessage(msg);
		//messagesRef.push().set(msg);
	};

	//messagesRef.set([]);

	this.getMessages = function () {
		$.get({
			url: '/api/messages',
			success: function(val, error){
				this.removeMessages();
				for(var i = 0; i < val.length; i++)
				{
					var messageJSON = val[i];
					var user = new User(messageJSON.user.firstName, messageJSON.user.lastName);
					var msg = new Message(messageJSON.txt, user, messageJSON.timestamp);
					this.showMessage(msg);
				}
			}.bind(this)
		})
	}

	this.removeMessages = function(){
		$('#message-list').empty();
	}

	this.showMessage = function (msg) {
		$('#message-list').append(
			$('<li>').addClass('clearfix').append(
				$('<div>').addClass('icon-column').append(
					$('<span>').addClass('user-icon').html('<abbr title="' + msg.user.fullName + '">' + msg.user.initials + '</abbr>'))
			).append(
				$('<div>').addClass('messages-column').append(
					$('<div>').addClass('message-info').append(
						$('<span>').addClass('user-name').html(msg.user.fullName)).append(
						$('<span>').addClass('timestamp').text(msg.displayTimestamp))
				).append(
					$('<p>').addClass('message').html(msg.displayTxt))   //.text vs .html
			)
		);
	}

	this.getMessages();


}

var messagesService = new MessagesService();

