function AuthenticationService(){
	var _currentUser = null;

	config.fb.authWithOAuthPopup("google", function(error, authData) {
		if (error) {
			console.log("Login Failed!", error);
		} else {
			console.log("Authenticated successfully with payload:", authData);
			this._currentUser = new User(authData.google.cachedUserProfile.given_name, authData.google.cachedUserProfile.family_name);
		}
	}.bind(this));

	this.__defineGetter__("currentUser", function(){
		return this._currentUser;
	})
}

var authenticationService = new AuthenticationService();
