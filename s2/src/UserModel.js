function User(firstName, lastName){
	this.firstName = firstName;
	this.lastName = lastName;
	this.fullName = this.firstName + " " + this.lastName; //Object literals
	this.__defineGetter__('initials', function(){
		return this.firstName.charAt(0) + this.lastName.charAt(0);
	})
}