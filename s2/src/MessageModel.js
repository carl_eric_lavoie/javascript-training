function Message(txt, user, timestamp){
	this.txt = txt;
	this.user = user;
	function _addAbbreviations(msg) {
		var words = msg.split(' ');

		for(var i = 0; i < words.length; i++)
		{
			var word = words[i];
			if(DICTIONARY[word.toLowerCase()]){
				word = '<abbr title="'+DICTIONARY[word.toLowerCase()]+'">'+word+'</abbr>';
				words[i] = word;
			}
		}

		//rebuild the string
		return words.join(' ');
	}

	this.__defineGetter__('displayTxt', function(){
		return _addAbbreviations(this.txt);
	})

	this.date = new Date(timestamp);

	this.__defineGetter__('displayTimestamp', function(){
		var hourSuffix = this.date.getHours()>12?'PM':'AM';
		var minutes = this.date.getMinutes()<10?'0'+this.date.getMinutes():this.date.getMinutes(),
				hours = this.date.getHours()%12;
		return hours+":"+minutes+" "+hourSuffix;
	});
}

