var express = require('express');
var app = express();
var path = require('path');


app.listen(3000, function () {
	console.log('Example app listening on port 3000!');
});

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/src/:name', function(req, res){
	var fileName = req.params.name;
	res.sendFile(path.join(__dirname + '/src/'+ fileName));
})

app.get('/styles/:name', function(req, res){
	var fileName = req.params.name;
	res.sendFile(path.join(__dirname + '/styles/'+ fileName));
})