/**
Introduction to object types
	Primitives : !object and has no methods
		string, number, boolean, null, undefined, symbol

	Wrappers : Adds methods and is an object. Value accessible through .valueOf();
		String, Number, Boolean, Symbol{ES6}

{ES6} Symbols
	-No constructor
 	-Created through func call : Symbol("iterator")
  	-Always creates a new one : Symbol("iterator") === Symbol("iterator") ->false
  	-Retrieve global Symbols : Symbol.for("bar") === Symbol.for("bar") -> true
  	-Used Symbols : Symbol.iterator -> used in "for of..." loops
  	-[][Symbol.iterator]
	-""[Symbol.iterator]

Everything else is an object.
**/


/**
Introduction to Functions

1.No return type
	function sayHello(){console.log('Hello world!'};
2.No name required
	function(){} -> invalid since there's no reference and it does nothing.
	(function(){}) -> valid
	{ES6} fat arrow functions -> Always keeps the same execution context. 'this' never changes
3.arguments not required, they are only used to map argument element to a name.

	functions sayHello(name){console.log('Hello ' + name + '!')}

	Arguments is "arrayish" meaning it looks like an array but it doesn't act like one.
	Array.from(arguments) -> ES6, otherwise you do traditional looping
	function sayHelloAll(){console.log('Hello ' + arguments.join())};
	function sayHelloAll(){console.log('Hello ' + Array.from(arguments).join())};

	{ES6}
	rest operator
	function sayHelloOthers(name, ...others){console.log(`Hello ${name} and to the others ${others.join(', ')}`)}
	rest operator returns an array : others instanceof Array -> true
**/

//Object Literals
var cel = {
	firstName : "Carl-Eric",
	lastName: "Lavoie",

	//define getter
	get fullName(){
		return this.firstName + ' ' + this.lastName;
	},

	get initials(){
		return this.firstName.charAt(0) + this.lastName.charAt(0);
	}
};

//Object constructor
function User(firstName, lastName){

	//Using static attribute User.count as ID
	//not using "this" with id makes it similar to a private java attribute
	id = User.count++;

	//"this.property" makes this property public
	this.firstName = firstName;
	this.lastName = lastName;
	this.fullName = this.firstName + " " + this.lastName; //Object literals

	//function in object rather than in prototype.
	//Works fine until we want to change the implementation
	//this.__defineGetter__('initials', function(){
	//	return this.firstName.charAt(0) + this.lastName.charAt(0);
	//});

	User.instances.push(this);
}

//Using function in prototype will be inherited by all Users
User.prototype.__defineGetter__('initials', function(){
	return this.firstName.charAt(0) + this.lastName.charAt(0);
})

//Similar to static modifier
User.count = 0;

//Keep track of all created users
User.instances = []; //only for demo purpose. Prevents garbage collection

/**
Prototype Definition
	A prototype is an object. All Objects inherit the properties from their respective prototype.
	In our example: User.prototype is an object, meaning it can have attribute and properties just like any other Objects.
	All instances of User (ie : new User()) inherit properties from the User.prototype.
	more info @ https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/prototype
 **/


var u1 = new User('John', 'Doe');
var u2 = new User('Matt', 'Damon');

var user = u2;

/**
	A trick to display list of objects in comprehensive way
	console.table(User.instances)
**/


/**
	console.log(user.initials);
	user.initials = "CEL";
	console.log(user.initials);

	typeof cel   => object
	typeof user  => object

	user instance User => true
	cel instanceof User => false

	instanceof looks at constructor type, while typeof returns the underlaying type.
**/
