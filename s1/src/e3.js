// Jquery document ready shorthand
$(function(){

	//On keypress on the input
	$('#message-input').on('keypress', function(e){

		//check that 'enter' was pressed
		if(e.which === 13)
		{
			//Get the txt value
			var txt = $(this).val();

			//reset the text field
			$(this).val('');

			//create a new DatedMessage object
			var message = new DatedMessage(txt);

			//append message to messages list using {ES6} string templates
			$('#message-list').append(
					`
			<li class="clearfix">
				<div class="icon-column">
					<span class="user-icon">
						<abbr title="${user.fullName}">${user.initials}</abbr>
					</span>
				</div>
				<div class="messages-column">
					<div class="message-info">
						<span class="user-name">${user.fullName}</span>
						<span class="timestamp">${message.displayTimestamp}</span>
					</div>
					<p class="message">${message.displayTxt}</p>
				</div>
			</li>
		`);
		}
	});
})



/*
	Done with Jquery rather than {ES6} string templates
 */
/*
function submitMessageJQuery() {
	var $messageInput = $('#message-input');
	var message = new DatedMessage($messageInput.val());
	$messageInput.val('');
	$('#message-list').append(
		$('<li>').addClass('clearfix')
			.append(
					$('<div>').addClass('icon-column')
							.append(
									$('<span>').addClass('user-icon').html('<abbr title="'+message.user.fullName+'">'+message.user.initials + '</abbr>'))
			)
			.append(
				$('<div>').addClass('messages-column')
					.append(
						$('<div>').addClass('message-info')
							.append(
								$('<span>').addClass('user-name').html(message.user.fullName))
							.append(
								$('<span>').addClass('timestamp').text(message.displayTimestamp))

					)
					.append(
							$('<p>').addClass('message').html(message.displayTxt))   //.text vs .html
			)


	);

}
*/


/*
 * Only with plain javascript
 */
/*
function submitMessageJS() {
	var $messageInput = document.querySelector('#message-input');

	var ele = document.createElement('li');
	ele.innerHTML = "<span class='user'> Carl: </span><span class='message'>" + addAbbreviations($messageInput.value) + " </span>";
	$messageInput.value = "";
	document.querySelector('#message-list').appendChild(ele);
}

$(function(){
	document.querySelector('#message-input-form').addEventListener('submit',
			function (e) {
				e.preventDefault(); //return false;
				submitMessageJQuery();
			}
	)

	document.querySelector('#message-input').addEventListener('keypress',
		function(e){
			if(e.which === 13){
				e.preventDefault();
				submitMessageJQuery();
			}
		})
})
*/
