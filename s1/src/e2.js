"use strict";
function Message(txt){
	this.txt = txt;
	this.user = user;
}

Message.prototype.addAbbreviations = function(msg) {
	/**
	 Strict mode
	 enforces some additional rules in JavaScript. Those rules are mostly bad synthax that will now throw runtime errors.
	 */


	//Split message into an Array of words
	var words = msg.split(' ');

	/**
	Introduction to JS Loops
		1-Traditional for loop
			for(var i = 0; i < words.length; i++)
			"var" in necessary to prevent leakage to window scope
			as of ES6 should us "let" rather than "var" for narrowed scope



		2-Do / while loops
			works just like in Java.

			while([condition])
				statement

			do
	 			statement
	 		while([condition])



		3-{ES6} For ... in
			Iterates over the enumerable properties of an object
			ie:
				var obj = {id:5, name:"Dummy object"}
				for(attr in obj){
					console.log(attr);
				}

			potential issue:
				var array = [1,2,3];
	 			array.total = function(){this.reduce((p,c)=>p+c)};

	 			for(num in array){
	 				console.log(num);
	 			}
	 			>1
	 			>2
	 			>3
	 			>total


		4-{ES6} for ... of
			Uses Symbol.iterator to fetch an iterator. This iterator is then used to iterate over the object.
			If the Symbol is not a valid iterator, it will return an error : obj[Symbol.iterator] is not a function.
			ie:
				var array = [1,3,4];
				for(num of array){
					console.log(num);
				}

				for(char of "string"){
					console.log(char);
				}

	 		potential issue:
				var students = {
					"DOEJ124123412": {
						lastName: "Doe",
						firstName:"John"
					},

					"DOEJ098765212": {
						lastName: "Doe",
						firstName:"Jane"
					},
				}

	 			for(student of students){
	 				...
	 			}

	 			>Uncaught TypeError: students[Symbol.iterator] is not a function
	 			solution : use a Map or define an iterator for your Object


		5- {ES6} forEach((val, index)) only implemented on specific Objects (Array, Map, Set)
			can be useful when used with arrow function.
	**/

	//  ES6 synthax, (of -> iterator, in -> index)
	//        for(var word of msg.split(' ')){
	//            console.log(word);
	//        }



	/*
		Iterate over all words and look if it's in the dictionnary. If it's add an abbreviation for it.
	 */
	for(var i = 0; i < words.length; i++)
	{
		var word = words[i];
		if(DICTIONARY[word.toLowerCase()]){
			word = '<abbr title="'+DICTIONARY[word.toLowerCase()]+'">'+word+'</abbr>';
			words[i] = word;
		}
	}

	//rebuild the string
	return words.join(' ');
}

Message.prototype.__defineGetter__('displayTxt', function(){
	return this.addAbbreviations(this.txt);
})

function DatedMessage(txt){
	Message.call(this, txt);
	this.date = new Date();
}


DatedMessage.prototype = Object.create(Message.prototype);

DatedMessage.prototype.__defineGetter__('displayTimestamp', function(){
	let hourSuffix = this.date.getHours()>12?'PM':'AM';
	const minutes = this.date.getMinutes()<10?'0'+this.date.getMinutes():this.date.getMinutes(),
			hours = this.date.getHours()%12;
	return `${hours}:${minutes} ${hourSuffix}`;
});





